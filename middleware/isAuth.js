export default function({app, redirect}){
    let status = app.store.state.auth.auth.isLoggedIn
    
    if (status) {
        redirect({name: 'systemlist'})
    }
}