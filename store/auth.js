export const state = () => ({
  auth: {
    isLoggedIn: null,
    user: null,
    message: null,
    token: null,
  },
  web_url: []
});

export const mutations = {
  setStatus(state, data) {
    state.auth.isLoggedIn = data;
  },
  setUser(state, data) {
    state.auth.user = data;
  },
  setMessage(state, data) {
    state.auth.message = data;
  },
  setToken(state, data) {
    state.auth.token = data;
  },
  setSsoUrl(state,data){
    state.web_url = data
  },
  setRole(state, data){
    state.auth.role = data
  }
};

export const actions = {
  async logIn({ dispatch }, data) {
    let response = await this.$axios.$post("/auth/login", data);
    return dispatch("setSso", response);
  },
  setSso({ commit, state,app }, data) {
    const token = data.data.access_token;
    const message = data.meta.message;
    const username = data.data.user_username;
    const user_id = data.data.user_id;
    const token_type = data.data.token_type;
    const token_expired = data.data.token_expired;
    const user_email = data.data.user_email;
    const user_group = data.data.user_group;
    const role_name = data.data.role_name;
    const web_url = data.data.system_url;
    this.$axios.setHeader(token,token_type)
    const screetCook = [
      {
        name: 'token',value: token
      },
      {
        name: 'username',value: username
      },
    ]
    if (token) {
      this.$cookies.setAll(screetCook)
      commit("setToken", token);
      commit("setMessage", message);
      commit("setUser", username);
      if (state.auth.token && state.auth.user) {
          commit("setStatus", true)
          commit("setSsoUrl",web_url )
          commit("setRole", user_group)
      }else{
        commit("setStatus", false)
      }
    } else {
      commit("setMessage", message);
    }
  },
  // async setUser({ commit }, data) {
  //   const token = data.data.token;
  //   const username = data.data.username;
  //   if (token) {
  //     commit("setToken", token);
  //     Cookies.set("authToken", token);
  //     this.$axios.setToken(token, data.data.token_type);
  //   } else {
  //     commit("setMessage", "login salah");
  //   }

  //   try {
  //     let user = await this.$repositories.user.showParams(username);
  //     if (user && token) {
  //       commit("setStatus", true);
  //     } else {
  //       commit("setStatus", false);
  //     }
  //     commit("setUser", user.data.data[0]);
  //     Cookies.set("username", user.data.data[0].username);
  //     commit("setMessage", user.data.meta.message);
  //   } catch (error) {
  //     this.$axios.setToken(false);
  //     commit("setUser", null);
  //     commit("setStatus", false);
  //     commit("setToken", null);
  //     commit("setMessage", user.data.meta.message);
  //     Cookies.remove("username");
  //     Cookies.remove("authToken");
  //   }
  // },
  // async logOut() {
  //   try {
  //   } catch (error) {
  //     console.log('error')
  //   }
  // },
};

export const getters = {};
