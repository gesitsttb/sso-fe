
export const state = () => ({
    systemlist: []
  })
    
  export const mutations = {
    SET_SYSTEM_LIST(state, data){
      state.systemlist = data
    }
  }
  
  export const actions = {
    async getSystemList ({ commit }) {
      try {
        const response = await this.$axios.$get('/auth/systemlist')
        commit('SET_SYSTEM_LIST', response.data)
      } catch (error) {
        
      }
      
      
    }
  }
  
  export const getters = {
  }
  