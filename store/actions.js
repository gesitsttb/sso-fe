export async function nuxtServerInit({ commit, state }, { req, res, app }) {
    let username = app.$cookies.get("username");
    let token = app.$cookies.get("token");
    if (token !== null && token !== false) {
      this.$axios.setToken(token, 'Bearer');
      if (username && token) {
        commit('auth/setStatus', true)
      }
      commit('auth/setToken', token)
      commit('auth/setUser', username)
    }
  }
  